using System;
using System.Drawing;

namespace ClassLibrary
{
    public class Cercle
    {
        private double _radi;
        int _codi;
        string _nom;
        Color _color;

        public Cercle(double radi, Color color, string nom, int codi)
        {
            _radi = radi;
            _color = color;
            _nom = nom;
            _codi = codi;
        }

        public double Perimetre()
        {
            return Math.PI * _radi * 2;
        }
        
        public double Area()
        {
            return Math.PI * Math.Pow(_radi, 2);
        }
        
        public override string ToString()
        {
            return "\n CIRCULO" + 
                   "\n Radio: " + _radi +
                   "\n Color: " + _color +
                   "\n Nombre: " + _nom +
                   "\n Codigo: " + _codi + "\n";
        }

        public void SetRadio(double radio)
        {
            _radi = radio;
        }

        public void SetName(string name)
        {
            _nom = name;
        }

        public void SetColor(Color color)
        {
            _color = color;
        }

        public void SetCodi(int codi)
        {
            _codi = codi;
        }

        public double GetRadio()
        {
            return _radi;
        }

        public string GetName()
        {
            return _nom;
        }

        public Color GetColor()
        {
            return _color;
        }

        public int GetCodi()
        {
            return _codi;
        }
    }
}