using System.Drawing;

namespace ClassLibrary
{
    public class FiguraGeometrica
    {
        int _codi;
        private string _nom;
        Color _color;

        public FiguraGeometrica(int codi, string nom, Color color)
        {
            _codi = codi;
            _nom = nom;
            _color = color;
        }

        public override string ToString()
        {
            return "\n Color: " + _color +
                   "\n Nombre: " + _nom +
                   "\n Codigo: " + _codi;
        }
        
        public void SetName(string name) { _nom = name; }
        
        public void SetColor(Color color) { _color = color; }

        public void SetCodi(int codi) { _codi = codi; }

        public string GetName() { return _nom; }
        
        public Color GetColor() { return _color; }
        
        public int GetCodi() { return _codi; }
    }
}