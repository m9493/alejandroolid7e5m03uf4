﻿using System.Drawing;

namespace ClassLibrary
{
    public class Rectangle
    {
        double _base;
        double _altura;
        int _codi;
        string _nom;
        Color _color;
        
        public Rectangle(double varBase, double altura, Color color, string nom, int codi)
        {
            _base = varBase;
            _altura = altura;
            _color = color;
            _nom = nom;
            _codi = codi;
        }
        
        public double Perimetre()
        {
            return _altura*2 + _base*2;
        }
        
        public double Area()
        {
            return _altura * _base;
        }
        
        public override string ToString()
        {
            return "\n RECTANGULO" +
                   "\n Base: " + _base +
                   "\n Altura: " + _altura +
                   "\n Color: " + _color +
                   "\n Nombre: " + _nom +
                   "\n Codigo: " + _codi + "\n";
        }

        public void SetBase(double varBase)
        {
            _base = varBase;
        }

        public void SetAltura(double altura)
        {
            _altura = altura;
        }

        public void SetName(string name)
        {
            _nom = name;
        }

        public void SetColor(Color color)
        {
            _color = color;
        }

        public void SetCodi(int codi)
        {
            _codi = codi;
        }

        public double GetBase()
        {
            return _base;
        }
        
        public double GetAltura()
        {
            return _altura;
        }

        public string GetName()
        {
            return _nom;
        }

        public Color GetColor()
        {
            return _color;
        }

        public int GetCodi()
        {
            return _codi;
        }
    }
}