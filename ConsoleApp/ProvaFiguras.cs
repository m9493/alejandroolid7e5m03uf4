using System;
using System.Drawing;
using ClassLibrary;
using Rectangle = ClassLibrary.Rectangle;

namespace ConsoleApp
{
    class ProvaFiguras
    {
        static void Main()
        {
            var cercle = new Cercle(10, Color.Aqua, "Circulo", 0);
            var triangle = new Triangle(10, 20, Color.Coral, "Rectangulo", 1);
            var rectangle = new Rectangle(10, 20, Color.Chartreuse, "Rectangulo", 1);

            Console.ForegroundColor = ConsoleColor.Cyan;
            PrintCercle(cercle);
            PrintRectangle(rectangle);
            PrintTriangle(triangle);
            ExGetSet(rectangle);
        }

        static void PrintCercle(Cercle cercle)
        {
            Console.WriteLine(cercle);
            PrintAreaCercle(cercle);
            PrintPerimetreCercle(cercle);
            KeyClear();
        }

        static void PrintRectangle(Rectangle rectangle)
        {
            Console.WriteLine(rectangle);
            PrintAreaRectangle(rectangle);
            PrintPerimetreRectangle(rectangle);
            KeyClear();
        }

        static void PrintTriangle(Triangle triangle)
        {
            Console.WriteLine(triangle);
            PrintAreaTriangle(triangle);
            KeyClear();
        }

        static void ExGetSet(Rectangle rectangle)
        {
            Console.WriteLine(" Ejemplo de Get y Set" +
                              "\n Altura Rectangulo: " + rectangle.GetAltura());
            rectangle.SetAltura(10000);
            Console.ReadKey();
            Console.WriteLine(" Altura Rectangulo: " + rectangle.GetAltura());
        }

        static void KeyClear()
        {
            Console.ReadKey();
            Console.Clear();
        }

        static void PrintAreaCercle(Cercle cercle)
        {
            Console.WriteLine(" AREA");
            Console.WriteLine(cercle.Area());
        }
        
        static void PrintPerimetreCercle(Cercle cercle)
        {
            Console.WriteLine(" PERIMETRO");
            Console.WriteLine(cercle.Perimetre());
        }

        static void PrintAreaTriangle(Triangle triangle)
        {
            Console.WriteLine(" AREA");
            Console.WriteLine(triangle.Area());
        }

        static void PrintAreaRectangle(Rectangle rectangle)
        {
            Console.WriteLine(" AREA");
            Console.WriteLine(rectangle.Area());
        }
        
        static void PrintPerimetreRectangle(Rectangle rectangle)
        {
            Console.WriteLine(" PERIMETRO");
            Console.WriteLine(rectangle.Perimetre());
        }
    }
}