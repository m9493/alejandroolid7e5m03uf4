﻿using System.Drawing;

namespace P2M03Uf4AlejandroOlid.Library;

class Cercle : FiguraGeometrica
{
    double _radi;
    
    public Cercle(double radi, Color color, string nom, int codi) : base(codi, nom, color)
    {
        _radi = radi;
        Codi = codi;
        Nom = nom;
        Color = color;
    }

    public double Perimeter()
    {
        return Math.PI * _radi * 2;
    }
        
    public override double Area()
    {
        return Math.PI * Math.Pow(_radi, 2);
    }
        
    public override string ToString()
    {
        return "\n CIRCULO" + 
               "\n Radio: " + _radi +
               "\n Color: " + Color +
               "\n Nombre: " + Nom +
               "\n Codigo: " + Codi + "\n";
    }

    public void SetRadio(double radio)
    {
        _radi = radio;
    }

    public void SetName(string name)
    {
        Nom = name;
    }

    public void SetColor(Color color)
    {
        Color = color;
    }

    public void SetCodi(int codi)
    {
        Codi = codi;
    }

    public double GetRadio()
    {
        return _radi;
    }

    public string GetName()
    {
        return Nom;
    }

    public Color GetColor()
    {
        return Color;
    }

    public int GetCodi()
    {
        return Codi;
    }
}