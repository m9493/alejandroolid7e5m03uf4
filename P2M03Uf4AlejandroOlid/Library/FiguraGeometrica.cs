﻿using System.Drawing;

namespace P2M03Uf4AlejandroOlid.Library;

abstract class FiguraGeometrica
{
    protected int Codi;
    protected string Nom;
    protected Color Color;
    
    protected FiguraGeometrica(int codi, string nom, Color color)
    {
        Codi = codi;
        Nom = nom;
        Color = color;
    }
    
    public override bool Equals(object? obj)
    {
        if (obj == null || GetType() != obj.GetType()) return false;
        
        var figura = (FiguraGeometrica) obj;
        return Codi == figura.Codi;
    }

    public string StringEquals(object? obj)
    {
        return "La figura geométrica son iguales: " + Equals(obj);
    }

    public override int GetHashCode()
    {
        // ReSharper disable once NonReadonlyMemberInGetHashCode
        return Codi;
    }

    public abstract double Area();
}