﻿using System.Drawing;

namespace P2M03Uf4AlejandroOlid.Library;

class Triangle : FiguraGeometrica
{
    double _base;
    double _altura;

    public Triangle(double varBase, double altura, Color color, string nom, int codi) : base(codi, nom, color)
    {
        _base = varBase;
        _altura = altura;
        Color = color;
        Nom = nom;
        Codi = codi;
    }

    public override double Area()
    {
        return _base * _altura / 2;
    }

    public override string ToString()
    {
        return "\n TRIANGULO" +
               "\n Base: " + _base +
               "\n Altura: " + _altura +
               "\n Color: " + Color +
               "\n Nombre: " + Nom +
               "\n Codigo: " + Codi + "\n";
    }

    public void SetBase(double varBase)
    {
        _base = varBase;
    }

    public void SetAltura(double altura)
    {
        _altura = altura;
    }

    public void SetName(string name)
    {
        Nom = name;
    }

    public void SetColor(Color color)
    {
        Color = color;
    }

    public void SetCodi(int codi)
    {
        Codi = codi;
    }

    public double GetBase()
    {
        return _base;
    }

    public double GetAltura()
    {
        return _altura;
    }

    public string GetName()
    {
        return Nom;
    }

    public Color GetColor()
    {
        return Color;
    }

    public int GetCodi()
    {
        return Codi;
    }
}