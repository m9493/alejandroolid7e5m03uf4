﻿using System.Drawing;
using P2M03Uf4AlejandroOlid.Library;
using Rectangle = P2M03Uf4AlejandroOlid.Library.Rectangle;

namespace P2M03Uf4AlejandroOlid
{
    class Program
    {
        static void Main()
        {
            var cercle1 = new Cercle(10, Color.Aqua, "Circulo", 1);
            var cercle2 = new Cercle(10, Color.Aqua, "Circulo", 2);
            var triangle1 = new Triangle(10, 20, Color.Coral, "Rectangulo", 1);
            var rectangle1 = new Rectangle(10, 20, Color.Chartreuse, "Rectangulo", 1);
            var otherRectangle1 = new Rectangle(10, 20, Color.Chartreuse, "Rectangulo", 1);

            Console.ForegroundColor = ConsoleColor.Cyan;
            
            Console.Write(
                cercle1.GetHashCode() + "\n" +
                cercle2.GetHashCode() + "\n" +
                cercle1.StringEquals(cercle2) + "\n" + //mismo tipo pero diferente codigo
                
                triangle1.GetHashCode() + "\n" +
                rectangle1.GetHashCode() + "\n" +
                rectangle1.StringEquals(triangle1) + "\n" + //mismo codigo pero diferente tipo
                
                rectangle1.GetHashCode() + "\n" +
                otherRectangle1.GetHashCode() + "\n" +
                rectangle1.StringEquals(otherRectangle1)); //mismo codigo y tipo
        }
    }
}          